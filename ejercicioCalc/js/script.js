function calc() {
	var numOp = parseInt(document.getElementById("numOp").value);
	var num1 = parseFloat(document.getElementById("num1").value);
	var num2 = parseFloat(document.getElementById("num2").value);

	var modal = document.getElementById("modal-body");

	switch (numOp) {
		case 1:
			var result = num1 + num2;
			var op = "Suma";
			var simb = "+";
			break;
		case 2:
			var result = num1 - num2;
			var op = "Resta";
			var simb = "-";
			break;
		case 3:
			var result = num1 * num2;
			var op = "Multiplicacion";
			var simb = "x";
			break;
		case 4:
			var result = (num1 / num2).toFixed(2);
			var op = "Division";
			var simb = "/";
			if (result == Infinity) {
				result = "ERROR! No se puede dividir por cero.";
			}
			break;
	}

	modal.innerHTML = `
			<table class='table'>
			<thead class="fw-bold">
			<tr>
			<td>Primer valor</td>
			<td>Operacion</td>
			<td>Segundo valor</td>
			</tr>
			</thead>
			<tbody class="fw-bold">
			<tr>
			<td>${num1}</td>
			<td>${op}</td>
			<td>${num2}</td>
			</tr>
			</tbody>
			</table>
			
			<div class='fw-bold text-center'><h1>${num1} ${simb} ${num2} = ${result}</h1></div>
			`;
	var table1 = document.getElementById("table1");

	table1.innerHTML = `
	<table
	id="table1"
	class="table table-primary caption-top table-striped"
>
	<caption id="caption">
		<h3 class="fw-bold"><i class="bi bi-calculator"></i>Tabla de resultado</h3>
	</caption>
	<thead>
		<tr>
			<td>Primer valor</td>
			<td>Operacion</td>
			<td>Segundo valor</td>
			<td>Resultado</td>
		</tr>
	</thead>
	<tbody class="fw-bold">
		<tr>
		<td>${num1}</td>
		<td>${op}</td>
		<td>${num2}</td>
		<td> ${result}</td>
		</tr>
	</tbody>
	<tfoot>
		<thead>
			<tr>
				<td>Primer valor</td>
				<td>Operacion</td>
				<td>Segundo valor</td>
				<td>Resultado</td>
			</tr>
		</thead>
	</tfoot>
</table>

	`;
}
