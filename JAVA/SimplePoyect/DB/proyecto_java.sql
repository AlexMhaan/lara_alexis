-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2022 at 11:50 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyecto_java`
--

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `apellido` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(264) COLLATE utf8_bin DEFAULT NULL,
  `telefono` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `email`, `telefono`, `password`) VALUES
(3, 'Maria', 'Benavidez', 'maria@benavidez.com', '46793158', NULL),
(4, 'Ana', 'Black', 'ana@black.com', '87895456', NULL),
(6, 'Pedro', 'Picapiedra', 'pedro@picapiedra.com', '15324865', 'asd'),
(7, 'Pablo', 'Picapiedra', 'pablo@picapiedra.com', ' - - - - -', '123'),
(8, 'Melisa', 'Leier', 'melisa@leier.com', ' - - - - -', 'asdf'),
(10, 'Matias', 'Urlich', 'matias@urlich.com', ' - - - - - ', NULL),
(11, 'Juan', 'Pelota', 'juan@pelota.com', '45698702', 'asd123'),
(12, 'Daniela', 'Orellano', 'daniela@orerallo.com', '65498731', '589'),
(13, 'Dario', 'Lopez', 'dario@lopez.com', '65478913', '$argon2id$v=19$m=1024,t=13,p=1$vQtGZvBoX6crESiIi51fuQ$DXCv3SH4FCIJ+taj6tyTdmP9wvIPWSY89LN5rNGolE0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
