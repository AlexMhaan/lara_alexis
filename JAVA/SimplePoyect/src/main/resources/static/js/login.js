
$(document).ready(function() {
// on ready
});

async function iniSesion() {
    let datos = {};

    datos.email = document.getElementById('InputEmail').value;

    datos.password = document.getElementById('InputPassword').value;

    const request = await fetch('api/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(datos)
    });
    const resp = await request.text();

    if (resp != 'FAIL'){
        localStorage.token = resp;
        localStorage.email = datos.email;
        window.location.href = 'usuarios.html'
    }else {
        alert('El usuario o la contraseña no coinciden');
    }


}
