// Call the dataTables jQuery plugin
$(document).ready(function() {
    cargarUsuarios();
  $('#dataTable').DataTable();
});


async function cargarUsuarios(){


    const request = await fetch('dataTable', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });
    const usuarios = await request.json();

    let listadoHtml = "";
    for (let usuario of usuarios){
        let usuarioHtml = '<td>88</td> <td>'
                        + usuario.nombre +' '+ usuario.apellido +'</td> <td>'
                        + usuario.email +'</td> <td>'+ usuario.telefono +'</td> <td><a href="#" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></a></td>';

        listadoHtml += usuarioHtml;
    }

    console.log(listadoHtml);

    document.querySelector('#dataTable tbody').outerHTML = listadoHtml;

}