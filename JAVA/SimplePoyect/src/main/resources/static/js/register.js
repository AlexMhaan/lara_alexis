
$(document).ready(function() {
// on ready
});

async function registrarUsuario(){
    let datos = {};
    datos.nombre = document.getElementById('FirstName').value;
    datos.apellido = document.getElementById('LastName').value;
    datos.email = document.getElementById('InputEmail').value;
    datos.telefono = document.getElementById('InputTelefono').value;
    datos.password = document.getElementById('InputPassword').value;

    let rePass = document.getElementById('RepeatPassword').value;
    if (rePass != datos.password){
        alert('Las contraseñas no coinciden!')
        return;
    }
    const request = await fetch('api/usuarios', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(datos)
    });
    alert("Te has registrado correctamente!!");
    window.location.href = 'login.html'
}
